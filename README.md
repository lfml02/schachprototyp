# Schachprototyp Seminararbeit "Komponenten einer Engine für Strategiespiele"

Es handelt sich bei dem Programm lediglich um einen Prototypen ohne grafische Benutzeroberfläche und die Standertkomponenten eines Schachcomputer möglichst einfach zu implementieren. Es gibt zudem noch einige Fehler.


## Bedienung
in der main-Methode der Klasse UI.java lässt sich das Spiel konfigurieren.
ALs Gui verwende ich eine einfache Konsolendarstellung. Weiße Figuren werden mit einem Großbuchstaben, schwarze Figuren mit einem Kleinbuchstaben dargestellt. Es können deutsche oder englische Abkürzungen für die Figurzeichen verwendet werden. 
Bei unverändertem seed können die durchgeführten Tests reproduziert werden.
Der String startposition gibt die Anfangsstellung mit den Abkürzungen der Figuren in eingestellter Sprache an.

## Spiel starten:
```java
play(new AlphaBetaEngine(board, true, true), 4, new Player(board, gui), 0);
```
Der play-Methode werden die Engines und die Suchtiefe übergeben. Es können im Konstruktor Komponenten wie Piece-Square-Tables und Quiesence-Suche aktiviert werden.
Für einen Spieler öffnet sich ein Eingabefenster, wo ein Feld z.b. 'e2' ausgewählt werden kann. Und anschließend ein Fenster für das Zielfeld z.b. 'e4'.

## Testdaten
Alle gesammelten Testdaten aller Spiele befinden sich in Schachengine_Tests.xlsx. Dazu gehören: 
- Anzahl Züge
- Gesamtzeit der Spiels
- durchschnittliche Zeit pro Zug
- Durch [chess.com](https://www.chess.com/) ermittelte Zuggenauigkeit beider Spieler
- Besondere Züge und Anmerkungen

Alle Spiele sind zusätzlich in einer Sammlung gespeichert:
- [Spiel gegen sich selbst](https://www.chess.com/c/pu8bXrke)
- [Spiel gegen MiniMax 3](https://www.chess.com/c/2Wc4No9fU)



