package simplechess;

public class Figure {
	static final Figure EMPTY = new Figure(0, 0);

	static final int[] values = {0, 1, 3, 3, 5, 9, 1000}; 
	
	static final int NONE = 0;
	static final int PAWN = 1;
	static final int KNIGHT = 2;
	static final int BISHOP = 3;
	static final int ROOK = 4;
	static final int QUEEN = 5;
	static final int KING = 6;
	
	static final boolean BLACK = false;
	static final boolean WHITE = true;
	int[][] moveDirs;
	
	int type;
	int color;
	
	static Figure create(char symbol) {
		int index = Board.charset.indexOf(symbol) - 6;
		if (index == 0) return EMPTY;
		return new Figure(Math.abs(index), index > 0 ? -1 : 1);
	}
	
	public Figure(int type, int color) {
		this.type = type;
		this.color = color;
		this.moveDirs = getMoveDirections(type);
	}
	
	public boolean colorEquals(boolean color) {
		return color ? this.color == -1 : this.color == 1;
	}
	
	public char getSymbol() {
		return Board.charset.charAt(color * type + 6);
	}
	public int getValue() {
		return color * values[type];
	}
	public boolean canMove(Figure other) {
		return color * other.color <= 0;
	}
	public boolean canCapture(Figure other) {
		return color * other.color < 0;
	}
	public boolean canSlide() {
		return canSlide[type - 2];
	}
	
	private static int[][] getMoveDirections(int id) {
		switch (id) {
			case PAWN: return new int[0][0];
			case KNIGHT: return new int[][] {{2, 2, 1,-1,-2,-2,-1, 1}, {1,-1,-2,-2,-1, 1, 2, 2}};
			case BISHOP: return new int[][] {{1, 1,-1,-1,}, {1,-1,-1, 1}};
			case ROOK: return new int[][] {{1, 0,-1, 0}, {0, 1, 0,-1}};
			case QUEEN:
			case KING: return new int[][] {{1, 0,-1, 0, 1, 1,-1,-1, }, {0, 1, 0,-1, 1,-1,-1, 1}};
			default: return null;
		}
	}

	private static final boolean[] canSlide = {false, true, true, true, false};
	
	static final int[][] pieceSquareTable = new int[][] {
		{
			5, 5, 5, 5, 5, 5, 5, 5,
			5, 5, 5, 5, 5, 5, 5, 5,
			3, 3, 3, 4, 4, 3, 3, 3,
			1, 1, 1, 3, 3, 1, 1, 1,
			0, 0, 1, 2, 2, 1, 0, 0,
		   -1,-1,-1,-2,-2,-1,-1,-1,
			1, 1, 0,-2,-1, 0, 1, 1,
			0, 0, 0, 0, 0, 0, 0, 0
		},{
			-5,-4,-3,-3,-3,-3,-4,-5,
			-4,-2, 0, 0, 0, 0,-2,-4,
			-3, 0, 1, 1, 1, 1, 0,-3,
			-3, 1, 1, 1, 1, 1, 1,-3,
			-3, 0, 1, 1, 1, 1, 0,-3,
			-3, 1, 1, 1, 1, 1, 1,-3,
			-4,-2, 0, 0, 0, 0,-2,-4,
			-5,-1,-3,-3,-3,-3,-1,-5,
		},{
			-2,-1,-1,-1,-1,-1,-1,-2,
			-1, 0, 0, 0, 0, 0, 0,-1,
			-1, 0, 1, 1, 1, 1, 0,-1,
			-1, 1, 1, 2, 2, 1, 1,-1,
			-1, 0, 2, 1, 1, 2, 0,-1,
			-1, 1, 1, 1, 1, 1, 1,-1,
			-1, 1, 0, 0, 0, 0, 1,-1,
			-2,-1,-1,-1,-1,-1,-1,-2,		
		},{
			1, 1, 1, 1, 1, 1, 1, 1,
			1, 2, 2, 2, 2, 2, 2, 1,
		   -1, 0, 0, 0, 0, 0, 0,-1,
		   -1, 0, 0, 0, 0, 0, 0,-1,
		   -1, 0, 0, 0, 0, 0, 0,-1,
		   -1, 0, 0, 0, 0, 0, 0,-1,
		   -1, 0, 0, 0, 0, 0, 0,-1,
			0,-1, 1, 2, 2, 1,-1, 0
		}, {
			-2,-1,-1,-1,-1,-1,-1,-2,
			-1, 0, 0, 0, 0, 0, 0,-1,
			-1, 0, 1, 1, 1, 1, 0,-1,
			-1, 0, 1, 1, 1, 1, 0,-1,
			 0, 0, 1, 1, 1, 1, 0,-1,
			-1, 1, 1, 1, 1, 1, 0,-1,
			-1, 0, 1, 0, 0, 0, 0,-1,
			-2,-1,-1,-1,-1,-1,-1,-2
		},{
			-3,-4,-4,-5,-5,-4,-4,-3,
			-3,-4,-4,-5,-5,-4,-4,-3,
			-3,-4,-4,-5,-5,-4,-4,-3,
			-3,-4,-4,-5,-5,-4,-4,-3,
			-2,-3,-3,-4,-4,-3,-3,-2,
			 0,-2,-2,-2,-2,-2,-2, 0,
			 2, 2, 0, 0, 0, 0, 2, 2,
			 2, 2, 1, 0, 0, 1, 2, 2
		}
	};
}
