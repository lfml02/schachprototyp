package simplechess;

public class Move {
	int from;
	int to;
	
	Figure captured;
	Figure converted;
	
	public Move(int from, int to) {
		this.from = from;
		this.to = to;
		this.captured = Figure.EMPTY;
		this.converted = Figure.EMPTY;
	}
}
