package simplechess;

import java.util.List;

public class MiniMaxEngine extends ChessEngine {
	boolean usePST;
	
	public MiniMaxEngine(Board board, boolean usePST) {
		super(board);
		this.usePST = usePST;
	}

	@Override
	public Move generateMove(boolean color, int depth) {
		long time = System.nanoTime();
	    Move bestMove = new Move(0, 0);
	    
	    int max = Integer.MIN_VALUE;
	    int score;
	    List<Move> moves = board.getLegalMoves(color);
	    
	    for (int i = 0; i < moves.size(); i++)  {
	    	board.make(moves.get(i));
	        score = -negaMaxReq(!color, depth - 1);
	        board.undo();
	        if(score > max) {
	        	max = score;
	        	bestMove = moves.get(i);
	        }
	    }
	 
		String move = board.board[bestMove.from].getSymbol() + " > " + ConsoleGui.getPos(bestMove.to);
		System.out.println("Eval: " + max + " Move: " + move + " Time: " + ((System.nanoTime() - time) / 1000000.0f) + "ms");
		return bestMove;
	}
	
	public int negaMaxReq(boolean color, int depth) {
	    if (depth == 0) {
	    	if (usePST) return board.evaluate(color);
	    	else return board.evaluateSimple(color);
	    }
	    
	    int max = Integer.MIN_VALUE;
	    int score;
	    List<Move> moves = board.getAllMoves(color, false);
	    
	    for (int i = 0; i < moves.size(); i++)  {
	    	board.make(moves.get(i));
	        score = -negaMaxReq(!color, depth - 1);
	        board.undo();
	        if (score > max) max = score;
	    }
	    return max;
	}
}
