package simplechess;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Stack;

public class Board {
	static final int OUTOFBOUNDS = -1;
	
	static String charset = ConsoleGui.symbolsEN;
	static long seed;
	Random r;
	static int size = 8;
	int whiteFigures;
	int blackFigures;
	int eval;
	
	Figure[] board;

	Stack<Move> moves;
	
	static int getPos(int x, int y) {
		if (x < 0 || x >= size || y < 0 || y >= size) return OUTOFBOUNDS;
		return y * size + x;
	}
	
	public Board(String position) {
		r = new Random(seed);
		moves = new Stack<>();
		board = new Figure[size * size];
		
		for (int i = 0; i < size * size; i++) {
			board[i] = Figure.create(position.charAt(i));
			if (board[i].colorEquals(Figure.WHITE)) whiteFigures++;
			else if (board[i].colorEquals(Figure.BLACK)) blackFigures++;
		}
	}
	
	Figure get(int x, int y) {
		if (getPos(x, y) == OUTOFBOUNDS) return Figure.EMPTY;
		return board[getPos(x, y)];
	}
	
	String getPGN(Move move) {
		String fig = ("" + ConsoleGui.symbolsEN.charAt(board[move.from].type + 6)).toUpperCase();
		return (fig.equals("P") ? (move.captured == Figure.EMPTY ? "" :  ConsoleGui.getPos(move.from).charAt(0)) : (fig + ConsoleGui.getPos(move.from))) + (move.captured != Figure.EMPTY ? "x" : "") + ConsoleGui.getPos(move.to) + (move.converted != Figure.EMPTY ? "=Q" : "");	
	}
	
	void make(Move move) {
		board[move.to] = board[move.from];
		board[move.from] = Figure.EMPTY;
		if (move.converted != Figure.EMPTY) board[move.to] = move.converted;
		if (move.captured.color == 1) blackFigures--;
		else if (move.captured.color == -1) whiteFigures--;
		moves.push(move);
		eval = eval - move.captured.getValue() + move.converted.getValue();
	}
	
	void undo() {
		Move move = moves.pop();
		board[move.from] = board[move.to];
		board[move.to] = move.captured;
		if (move.converted != Figure.EMPTY) board[move.from] = new Figure(Figure.PAWN, move.converted.color);
		if (move.captured.color == 1) blackFigures++;
		else if (move.captured.color == -1) whiteFigures++;
		eval = eval + move.captured.getValue() - move.converted.getValue();
	}
	
	int evaluateSimple(boolean color) {
		return (color ? -eval : eval) + r.nextInt(11) - 5;
	}
	int evaluate(boolean color) {
		int eval = 0;
		List<Integer> positions = getPositions(Figure.WHITE);
		int pos;
		for (int i = 0; i < positions.size(); i++) {
			pos = positions.get(i);
			eval += board[pos].getValue() * 10;
			eval -= Figure.pieceSquareTable[board[pos].type - 1][pos];
		}
		positions = getPositions(Figure.BLACK);
		for (int i = 0; i < positions.size(); i++) {
			pos = positions.get(i);
			eval += board[pos].getValue() * 10;
			eval += Figure.pieceSquareTable[board[pos].type - 1][(size * size - (pos + 1))];
		}
		return (color ? -eval : eval) + r.nextInt(3) - 1;
	}
	
	public int checkWin(boolean color) {
		List<Move> legalMoves = getLegalMoves(!color);
		if (legalMoves.size() == 0 && inCheck(!color)) return 1;
		else if(legalMoves.size() == 0 || (whiteFigures == 1 && blackFigures == 1)) return 0;
		else return -1;
	}
	
	List<Integer> getPositions(boolean color) {
		ArrayList<Integer> list = new ArrayList<>();
		int pos;
		for (int i = 0; i < size * size; i++) {
			pos = color ? 63 - i : i; 
			if (board[pos].colorEquals(color)) {
				list.add(pos);
				if (color && list.size() == whiteFigures || !color && list.size() == blackFigures) break;
			}
		}
		return list;
	}
	
	List<Move> getLegalMoves(boolean color) {
		List<Move> moves = getAllMoves(color, false);
		ArrayList<Move> legalMoves = new ArrayList<>();
		for (int i = 0; i < moves.size(); i++) {
			make(moves.get(i));
			if (!inCheck(color)) legalMoves.add(moves.get(i));
			undo();
		}
		return legalMoves;
	}
	boolean inCheck(boolean color) {
		List<Move> moves = getAllMoves(!color, false);
		for (int i = 0; i < moves.size(); i++) {
			if (moves.get(i).captured.type == Figure.KING) return true;
		}
		return false;
	}
	
	List<Move> getAllMoves(boolean color, boolean onlyCaptures) {
		List<Integer> figures = getPositions(color);
		ArrayList<Move> moves = new ArrayList<>();
		
		for (int i = 0; i < figures.size(); i++) {
			moves.addAll(getMoves(figures.get(i) % size, figures.get(i) / size, onlyCaptures));
		}
		return moves;
	}
	
	List<Move> getMoves(int pos, boolean onlyCaptures) {
		return getMoves(pos % size, pos / size, onlyCaptures);
	}
	
	private List<Move> getMoves(int x, int y, boolean onlyCaptures) {
		ArrayList<Move> moves = new ArrayList<>();
		ArrayList<Move> captures = new ArrayList<>();
		
		int thisPos = getPos(x, y);
		Figure figure = board[thisPos];
		Move move;
		int pos;
		
		if (figure.type == Figure.PAWN) {
			pos = getPos(x, y + figure.color);
			if (board[pos] == Figure.EMPTY) {
				move = new Move(thisPos, pos);
				if (pos / size == 0 || pos / size == size - 1) move.converted = new Figure(Figure.QUEEN, figure.color);
				moves.add(move);
				if (y == 1 && figure.colorEquals(Figure.BLACK) || y == Board.size - 2 && figure.colorEquals(Figure.WHITE)) {
					pos = getPos(x, y + 2 * figure.color);
					if (board[pos] == Figure.EMPTY) moves.add(new Move(thisPos, pos));
				}	
			}
			for (int i = -1; i <= 1; i+= 2) {
				pos = getPos(x + i, y + figure.color);
				if (pos != OUTOFBOUNDS && figure.canCapture(board[pos])) {
					move = new Move(thisPos, pos);
					move.captured = board[pos];
					if (pos / size == 0 || pos / size == size - 1) move.converted = new Figure(Figure.QUEEN, figure.color);
					captures.add(move);
				}
			}
		}
		else {
			for (int dir = 0; dir < figure.moveDirs[0].length; dir++) {
				for (int step = 1; step == 1 || figure.canSlide(); step++) {
					pos = getPos(x + step * figure.moveDirs[0][dir], y + step * figure.moveDirs[1][dir]);
					if (pos == OUTOFBOUNDS) break;
					if (figure.canMove(board[pos])) {
						move = new Move(thisPos, pos);
						if (figure.canCapture(board[pos])) {
							move.captured = board[pos];
							captures.add(move);
							break;
						}
						moves.add(move);
					}
					else break;
				}
			}
		}
		if (!onlyCaptures) captures.addAll(moves);
		return captures;
	}
}
