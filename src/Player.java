package simplechess;

import java.util.List;

public class Player extends ChessEngine {
	ConsoleGui gui;
	
	public Player(Board board, ConsoleGui gui) {
		super(board);
		this.gui = gui;
	}

	@Override
	public Move generateMove(boolean color, int depth) {
		List<Move> legalMoves = board.getLegalMoves(color);
		
		boolean[] figures = new boolean[Board.size * Board.size];
		for (int i = 0; i < legalMoves.size(); i++) {
			figures[legalMoves.get(i).from] = true;
		}
		int from = gui.inputPos(color, figures, "Figur w�hlen");
		boolean[] moves = new boolean[Board.size * Board.size];
		for (int i = 0; i < legalMoves.size(); i++) {
			if (legalMoves.get(i).from == from) {
				moves[legalMoves.get(i).to] = true;
			}
		}
		int to = gui.inputPos(color, moves, "Zug w�hlen");
		Move move = new Move(from, to);
		move.captured = board.board[to];
		if (to / Board.size == 0 || to / Board.size == Board.size - 1) move.converted = new Figure(Figure.QUEEN, board.board[from].color);
		return move;
	}
}
