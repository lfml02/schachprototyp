package simplechess;

import java.util.List;

public class RandomEngine extends ChessEngine{
	
	public RandomEngine(Board board) {
		super(board);
	}
	
	@Override
	public Move generateMove(boolean color, int depth) {
		List<Move> moves = board.getLegalMoves(color);
		return moves.get(board.r.nextInt(moves.size()));
	}
}
