package simplechess;

public class UI {
	static final boolean BLACK = false;
	static final boolean WHITE = true;
	
	static Board board;
	static ConsoleGui gui;
	
	public static void main(String[] args) throws InterruptedException {
		Board.size = 8;
		Board.charset = ConsoleGui.symbolsDE;
		Board.seed = 12345678;
		
		String startposition = 
				"TSLKDLST" + 
				"BBBBBBBB" +
				"        " +
				"        " +
				"        " +
				"        " +
				"bbbbbbbb" +
				"tslkdlst";
		
		board = new Board(startposition);
		gui = new ConsoleGui(board);
		
		play(new AlphaBetaEngine(board, true, true), 4, new Player(board, gui), 0);
	}
	
	static void play(ChessEngine white, int whiteDepth, ChessEngine black, int blackDepth) throws InterruptedException {
		long t;
		long timeWhite = 0;
		long timeBlack = 0;
		String pgn = "";
		Move move;
		int counter = 1;
		
		while (counter <= 100 && timeWhite / 1000000000.0 < 300 && timeBlack / 1000000000.0 < 300) {
			t = System.nanoTime();
			move = white.generateMove(WHITE, whiteDepth);
			timeWhite += System.nanoTime() - t;
			pgn += " " + counter + ". " + board.getPGN(move);
			board.make(move);
			ConsoleGui.print(WHITE);
			System.out.println("Move " + (counter * 2 - 1) + " Time: " + (timeWhite / 1000000000.0) + "s avg: " + ((timeWhite / counter) / 1000000.0) + "ms");
			System.out.println(pgn + "\n");
			if (checkWin(WHITE)) break;
			
			t = System.nanoTime();
			move = black.generateMove(BLACK, blackDepth);
			timeBlack += System.nanoTime() - t;
			pgn += " " + board.getPGN(move);
			board.make(move);
			ConsoleGui.print(WHITE);
			System.out.println("Move " + counter * 2 + " Time: " + (timeBlack / 1000000000.0) + "s avg: " + ((timeBlack / counter) / 1000000.0) + "ms");
			System.out.println(pgn + "\n");
			if (checkWin(BLACK)) break;
			counter++;
		}
		System.out.println("Timesum: " + ((timeWhite + timeBlack) / 1000000000.0) + "s avg: " + (((timeWhite + timeBlack) / counter) / 1000000.0) + "ms");
	}
	
	static boolean checkWin(boolean color) {
		int win = board.checkWin(color);
		if (win == 1) {
			System.out.println((color? "White" : "Black") + " wins!");
			return true;
		}
		if (win == 0) {
			System.out.println("Game is drawn!");
			return true;
		}
		return false;
	}
}
