package simplechess;
import javax.swing.JOptionPane;

public class ConsoleGui {
	public static final String symbolsEN = "KQRBNP pnbrqk";
	public static final String symbolsDE = "KDTLSB bsltdk";
	public static final String symbolsUnicode = "♚♛♜♞�?♟ ♙♗♘♖♕♔";
	
	static Board board;
	
	public ConsoleGui(Board board) {
		ConsoleGui.board = board;
	}
	
	static void print(boolean color) {
		print(new boolean[Board.size * Board.size], color);
	}
	static void print(boolean[] selection, boolean color) {
		prt("  " + mulStr("_", Board.size * 2) + "\n");
	
		if (!color) {
			for (int j = Board.size - 1; j >= 0 ; j--) {
				prt(8 - j + "|");
				for (int i = 0; i < Board.size; i++) {
					prtSymbol(board.get(i, j).getSymbol(), selection[j * Board.size + i]);
				}
				prt("|\n");
			}
		}
		else {
			for (int j = 0; j < Board.size; j++) {
				prt(8 - j + "|");
				for (int i = Board.size - 1; i >= 0 ; i--) {
					prtSymbol(board.get(i, j).getSymbol(), selection[j * Board.size + i]);
				}
				prt("|\n");
			}
		}
		prt("  " + mulStr("_", Board.size * 2));
		prt("  W: " + board.whiteFigures + " B: " + board.blackFigures + "\n");
		prt("  " + getAlphabet(!color, Board.size) + "\n");
		try{Thread.sleep(100);}
		catch(InterruptedException e) {e.printStackTrace();}
	}
	private static void prtSymbol(char symbol, boolean selected) {
		prt(symbol == ' ' ? (selected ? "* " : ". ") : symbol + (selected ? "!" : " "));
	}
	private static void prt(String text) {
		System.out.print(text);
	}
	private static String mulStr(String str, int mul) {
		String s = str;
		for (int i = 0; i < mul - 1; i++) s += str;
		return s;
	}
	private static String getAlphabet(boolean reverse, int to) {
		String s = "";
		if (reverse) {
			for (char i = (char)('A' + to - 1); i >= 'A'; i--) s += i + " ";
		}
		else {
			for (char i = 'A'; i < ('A' + to); i++) s += i + " ";
		}
		return s;
	}
	
	int inputPos(boolean color, boolean[] fields, String message) {
		print(fields, color);
		String input = JOptionPane.showInputDialog(message);
		/*while (!input.matches("/^[a-h][1-8]$/")) {
			input = JOptionPane.showInputDialog("Ungültige Eingabe. " + message);
		}*/
		int pos = toPos(input);
		while (wrongPos(fields, pos)) {
			input = JOptionPane.showInputDialog("Falsches Feld. " + message);
			/*while (!input.matches("/^[a-h][1-8]$/")) {
				input = JOptionPane.showInputDialog("Ungültige Eingabe. " + message);
			}*/
			pos = toPos(input);
		}
		return pos;
	}
	
	boolean wrongPos(boolean[] fields, int pos) {
		if (pos < 0 || pos >= 64) return true;
		for (int i = 0; i < 64; i++) {
			if (!fields[pos]) return true;
		}
		return false;
	}
	
	int toPos(String input) {
		input = input.toLowerCase();
		return (7 - (input.charAt(1) - '1')) * Board.size + 7 - (input.charAt(0) - 'a');
	}
	
	public static String getPos(int field) {
		return "" + (char)(7 - field % Board.size + 'a') + (char)(7 - field / Board.size + '1');
	}
}
