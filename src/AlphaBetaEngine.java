package simplechess;

import java.util.List;

public class AlphaBetaEngine extends ChessEngine {
	boolean usePST;
	boolean quiesce;
	
	public AlphaBetaEngine(Board board, boolean usePST, boolean quiesce) {
		super(board);
		this.usePST = usePST;
		this.quiesce = quiesce;
	}

	@Override
	public Move generateMove(boolean color, int depth) {
		long time = System.nanoTime();
	    Move bestMove = new Move(0, 0);
	    
	    int score;
	    int alpha = Integer.MIN_VALUE + 1;
	    int beta = Integer.MAX_VALUE - 1;
	    List<Move> moves = board.getLegalMoves(color);
	    
	    for (int i = 0; i < moves.size(); i++)  {
	    	board.make(moves.get(i));
	        score = -alphaBetaReq(!color, -beta, -alpha, depth - 1);
	        board.undo();
	        if (score >= beta) {
	        	alpha = score;
	        	bestMove = moves.get(i);
	        	break;
	        }
	        if (score > alpha) {
	        	alpha = score;
	        	bestMove = moves.get(i);
	        }
	    }
	 
		String move = board.board[bestMove.from].getSymbol() + " > " + ConsoleGui.getPos(bestMove.to);
		System.out.println("Eval: " + alpha + " Move: " + move + " Time: " + ((System.nanoTime() - time) / 1000000.0) + "ms");
		return bestMove;
	}
	
	int alphaBetaReq(boolean color, int alpha, int beta, int depth) {
	    if (depth == 0) {
	    	if (quiesce) return quiesce(color, alpha, beta, 4);
	    	if (usePST)  return board.evaluate(color);
	    	else return board.evaluateSimple(color);
	    }
	    
	    int score;
	    List<Move> moves = board.getAllMoves(color, false);
	    
	    for (int i = 0; i < moves.size(); i++)  {
	    	board.make(moves.get(i));
	        score = -alphaBetaReq(!color, -beta, -alpha, depth - 1);
	        board.undo();
	        if (score >= beta) return beta;
	        if (score >= alpha) alpha = score;
	    }
	    return alpha;
	}
	
	int quiesce(boolean color, int alpha, int beta, int depth) {
		int eval;
		if (usePST) eval = board.evaluate(color);
		else eval = board.evaluateSimple(color);
			
		if (depth == 0) return eval;
	    if (eval >= beta) return beta;
	    if (alpha < eval) alpha = eval;
	    
	    int score;
	    List<Move> moves = board.getAllMoves(color, true);
	    
	    for (int i = 0; i < moves.size(); i++)  {
	    	board.make(moves.get(i));
	        score = -quiesce(!color, -beta, -alpha, depth - 1);
	        board.undo();
	        if (score >= beta) return beta;
	        if (score >= alpha) alpha = score;
	    }
	    return alpha;
	}
}
