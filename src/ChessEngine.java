package simplechess;

public abstract class ChessEngine {
	Board board;
	
	public ChessEngine(Board board) {
		this.board = board;
	}
	
	public abstract Move generateMove(boolean color, int depth);
}
